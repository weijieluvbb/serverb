# Server B
This repo is part of a [medium article](https://medium.com/@datails/end-to-end-test-microservices-using-docker-and-gitlab-ci-53119c2fad89). See the image in the private [GitLab registry](https://gitlab.com/m6093/server-b/-/packages).

## Start app
```bash
npm run start
curl http://localhost:3000 # => [{ ... }]
```